---
weight: 100
title: Blocking Ads in VRChat
---

Ads are annoying, bloated, and wasteful of your time and attention.
They are found prominently in popular worlds, luckily the prefabs can be blocked by the end user with this handy git project:

![AdGoBye](https://raw.githubusercontent.com/AdGoBye/AdGoBye/main/Marketing/lsmbeforeafter.webp)

https://github.com/AdGoBye/AdGoBye


In-game analytics can also be blocked at the DNS level, these are often external services the game phones into to deliver usage statistics. Opt out by blocking this domain list in your network or computer firewall/ DNS service.

https://github.com/Luois45/VRChatAnalyticsBlocklist