---
weight: 300
title: ALVR
---

# ALVR

- [ALVR GitHub repository](https://github.com/alvr-org/ALVR)

> Stream VR games from your PC to your headset via Wi-Fi.

Feature rich and open source, ALVR currently depends on the SteamVR runtime, if you are looking to utilize wireless streaming of OpenXR without SteamVR, consider using [WiVRn](/docs/fossvr/wivrn/).
