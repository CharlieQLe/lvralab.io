#!/bin/bash

TRAILING_SPACES=$(find content -name '*.md' -exec grep -EHn ' +$' {} \;)

if [[ -n "$TRAILING_SPACES" ]]; then
    echo "Error: these files have trailing spaces!"
    echo "$TRAILING_SPACES"
    exit 1
fi
